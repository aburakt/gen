<?php

namespace Abturan\Gen;

use Illuminate\Support\ServiceProvider;

class GenServiceProvider extends ServiceProvider
{

    protected $commands = [
        'Abturan\Gen\Commands\Gen',
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands($this->commands);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
